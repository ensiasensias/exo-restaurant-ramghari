package com.nespresso.sofa.recruitment.restaurant.types;

public enum Unit {
	
	Kg("Kg"),
	
	cl("cl"),
	
	g("g"),
	
	NONE("");

	private String name;

	private Unit(String name) {
		this.name = name;
	}
	
	public static Unit getUnitByName(String unitName) {
		Unit[] values = Unit.values();

		for (Unit unit : values) {
			if (unit.name.equals(unitName)) {
				return unit;
			}
		}
		throw new IllegalArgumentException();
	}

}
