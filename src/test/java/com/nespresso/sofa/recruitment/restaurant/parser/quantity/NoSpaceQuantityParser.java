package com.nespresso.sofa.recruitment.restaurant.parser.quantity;

import com.nespresso.sofa.recruitment.restaurant.models.Quantity;
import com.nespresso.sofa.recruitment.restaurant.types.Unit;

public class NoSpaceQuantityParser implements QuantityParser {

	//exemple: 6 or 1Kg 
	@Override
	public Quantity parse(String quantityInfo) {
		StringBuilder quantity=new StringBuilder();
		StringBuilder unitName=new StringBuilder();
		
		for(int i=0;i<quantityInfo.length();i++){
			try{
				Integer number=Integer.valueOf(String.valueOf(quantityInfo.charAt(i)));
				quantity.append(number);
			}
			catch(Exception e){
				unitName.append(quantityInfo.charAt(i));
			}
		}
		Unit unit=null;
		try{
			unit=Unit.getUnitByName(unitName.toString());
			return new Quantity(Integer.valueOf(quantity.toString()), unit);
		}catch(IllegalArgumentException e){
			return new Quantity(null, null);
		}
	}

}
