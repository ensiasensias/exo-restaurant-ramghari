package com.nespresso.sofa.recruitment.restaurant.types;

public enum Ingeredient {

	ballsMozzarella("balls Mozzarella"),

	tomatoes("tomatoes"),

	oliveOil("olive oil"),

	pepper("pepper"),
	
	seaSalt("sea salt"),
	
	water("water"),
	
	Flour("Flour");

	private String name;

	private Ingeredient(String name) {
		this.name = name;
	}

	public static Ingeredient getIngredientByName(String name){
		Ingeredient[] values=Ingeredient.values();
		
		for (Ingeredient ingredient : values) {
			if(ingredient.name.equals(name)){
				return ingredient;
			}
		}
		throw new IllegalArgumentException();
	}
}
