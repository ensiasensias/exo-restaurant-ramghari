package com.nespresso.sofa.recruitment.restaurant;

import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.models.Quantity;
import com.nespresso.sofa.recruitment.restaurant.parser.recipe.RecipeParser;
import com.nespresso.sofa.recruitment.restaurant.parser.recipe.SpaceRecipeParser;
import com.nespresso.sofa.recruitment.restaurant.types.Ingeredient;

public class Ticket {
	
	private Meal meal;
	private RecipeParser recipeParser=new SpaceRecipeParser();

	public Ticket() {
		super();
	}

	public void addRecipe(String recipeInfo) {
		this.meal = recipeParser.parse(recipeInfo);
	}
	
	public Meal getMeal() {
		return meal;
	}

	public boolean canBeExecute(Map<Ingeredient, Quantity> stock) {
		return meal.canBeExecute(stock);
	}

	public Ticket and(String recipesInfo) {
		Meal meal=recipeParser.parse(recipesInfo);
		this.meal.addMealPart(meal);
		return this;
	}

}
