package com.nespresso.sofa.recruitment.restaurant.parser.stock;

import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.models.Quantity;
import com.nespresso.sofa.recruitment.restaurant.types.Ingeredient;

public interface StockParser {
	
	public Map<Ingeredient, Quantity> parse(String ... stock);

}
