package com.nespresso.sofa.recruitment.restaurant.preparators;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.models.IngredientQuantity;
import com.nespresso.sofa.recruitment.restaurant.models.Quantity;
import com.nespresso.sofa.recruitment.restaurant.types.Ingeredient;

public abstract class  Preparator {
	
	protected List<IngredientQuantity> ingredient;
	protected Integer bakedDuration;
	protected Integer cookingDuration;
	
	public Preparator(Integer cookingDuration) {
		super();
		this.ingredient=new ArrayList<IngredientQuantity>();
		this.cookingDuration=cookingDuration;
	}
	public Preparator(Integer cookingDuration, Integer bakedDuration) {
		super();
		this.ingredient=new ArrayList<IngredientQuantity>();
		this.bakedDuration=bakedDuration;
		this.cookingDuration=cookingDuration;
	}

	public abstract Integer cookingDuration(Integer quantity);

	public boolean canPrepare(Map<Ingeredient, Quantity> stock,
			Integer recipeOrderedQuantity) {
		for (IngredientQuantity ingredientQuantity : ingredient) {
			if (!ingredientQuantity.canBeFoundInStock(stock,
					recipeOrderedQuantity)) {
				return false;
			}
		}
		return true;
	}
}
