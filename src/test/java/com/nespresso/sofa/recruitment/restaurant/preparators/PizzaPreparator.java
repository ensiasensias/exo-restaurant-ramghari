package com.nespresso.sofa.recruitment.restaurant.preparators;

import com.nespresso.sofa.recruitment.restaurant.models.IngredientQuantity;
import com.nespresso.sofa.recruitment.restaurant.models.Quantity;
import com.nespresso.sofa.recruitment.restaurant.types.Ingeredient;
import com.nespresso.sofa.recruitment.restaurant.types.Unit;

public class PizzaPreparator extends Preparator {

	public PizzaPreparator(Integer cookingDuration) {
		super(cookingDuration);
		ingredient.add(new IngredientQuantity(Ingeredient.ballsMozzarella,
				new Quantity(1, Unit.NONE)));
		ingredient.add(new IngredientQuantity(Ingeredient.tomatoes, new Quantity(
							4, Unit.NONE)));
		ingredient.add(new IngredientQuantity(Ingeredient.oliveOil, new Quantity(
				null, Unit.NONE)));
		ingredient.add(new IngredientQuantity(Ingeredient.water, new Quantity(100,
							Unit.cl)));
		
		ingredient.add(new IngredientQuantity(Ingeredient.Flour, new Quantity(300,
							Unit.g)));
		
		ingredient.add(new IngredientQuantity(Ingeredient.seaSalt, new Quantity(
							null, Unit.NONE)));
	}
	
	public PizzaPreparator(Integer cookingDuration,Integer bakedDuration) {
		super(cookingDuration, bakedDuration);
	}
	

	@Override
	public Integer cookingDuration(Integer quantity) {
		Integer cookingDurationResutl = (quantity * (cookingDuration / 2));
		cookingDurationResutl += (cookingDuration / 2);
		return cookingDurationResutl+this.bakedDuration;
	}

}
