package com.nespresso.sofa.recruitment.restaurant;

import java.util.HashMap;
import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.models.Quantity;
import com.nespresso.sofa.recruitment.restaurant.types.Ingeredient;
import com.nespresso.sofa.recruitment.restaurant.types.Recipe;

public class Meal {

	private Map<Recipe, Integer> recipes;

	public Meal() {
		super();
		recipes = new HashMap<Recipe, Integer>();
	}

	public void addRecipeWithQuantity(Recipe recipe, Integer quantity) {
		this.recipes.put(recipe, quantity);
	}

	public Integer servedDishes() {
		Integer quantity = 0;
		for (Recipe recipe : recipes.keySet()) {
			quantity += recipes.get(recipe);
		}
		return quantity;
	}

	public String cookingDuration() {
		Integer cookingDurationTotal=new Integer(0);
		for (Recipe recipe : recipes.keySet()) {
			cookingDurationTotal+= recipe.cookingDuration(recipes.get(recipe));
		}
		return String.valueOf(cookingDurationTotal);
	}

	public boolean canBeExecute(Map<Ingeredient, Quantity> stock) {
		for (Recipe recipe : recipes.keySet()) {
			if(!recipe.canBeExecuted(stock,recipes.get(recipe))){
				return false;
			}
		}
		return true;
	}

	public void addMealPart(Meal meal) {
		recipes.putAll(meal.recipes);
	}

}
