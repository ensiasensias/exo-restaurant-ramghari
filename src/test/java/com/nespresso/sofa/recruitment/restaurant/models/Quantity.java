package com.nespresso.sofa.recruitment.restaurant.models;

import com.nespresso.sofa.recruitment.restaurant.types.Unit;

public class Quantity {

	private Integer quantity;
	private Unit unit;

	public Quantity(Integer quantity, Unit unit) {
		super();
		this.quantity = quantity;
		this.unit = unit;
	}

	public boolean isUnlimitedOrUncomptable() {
		return this.quantity == null;
	}

	public boolean hasEnough(Quantity ingredientInStock,
			Integer recipeOrderedQuantity) {
		if (this.quantity * recipeOrderedQuantity > ingredientInStock.quantity) {
			return true;
		}
		return false;
	}

}
