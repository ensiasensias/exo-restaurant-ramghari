package com.nespresso.sofa.recruitment.restaurant.models;

import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.types.Ingeredient;

public class IngredientQuantity {

	private Ingeredient ingredient;
	private Quantity quantity;

	public IngredientQuantity(Ingeredient ingredient, Quantity quantity) {
		super();
		this.ingredient = ingredient;
		this.quantity = quantity;
	}

	public boolean canBeFoundInStock(Map<Ingeredient, Quantity> stock,
			Integer recipeOrderedQuantity) {
		Quantity ingredientInStock = stock.get(this.ingredient);
		if (quantity.isUnlimitedOrUncomptable()
				| ingredientInStock.isUnlimitedOrUncomptable()) {
			return true;
		}
		if (quantity.hasEnough(ingredientInStock,recipeOrderedQuantity)) {
			return false;
		}
		return true;
	}
}
