package com.nespresso.sofa.recruitment.restaurant.parser.stock;

import java.util.HashMap;
import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.models.Quantity;
import com.nespresso.sofa.recruitment.restaurant.parser.quantity.NoSpaceQuantityParser;
import com.nespresso.sofa.recruitment.restaurant.parser.quantity.QuantityParser;
import com.nespresso.sofa.recruitment.restaurant.types.Ingeredient;

public class SpaceStockParser implements StockParser {

	private static final String SPACE = " ";
	private QuantityParser quantityParser = new NoSpaceQuantityParser();

	@Override
	public Map<Ingeredient, Quantity> parse(String... stock) {
		Map<Ingeredient, Quantity> result = new HashMap<Ingeredient, Quantity>();
		for (String stockElement : stock) {
			prepareIngrediantQuantity(result, stockElement);
		}
		return result;
	}

	private void prepareIngrediantQuantity(Map<Ingeredient, Quantity> result,
			String stockElement) {
		String elements[] = stockElement.split(SPACE);
		Quantity quantity = quantityParser.parse(elements[0]);
		StringBuilder ingredientName = new StringBuilder();
		ingredientName = parseIngredientName(stockElement, elements, quantity);
		Ingeredient ingredient = Ingeredient.getIngredientByName(ingredientName
				.toString());
		result.put(ingredient, quantity);
	}

	private StringBuilder parseIngredientName(String stockElement,
			String[] elements, Quantity quantity) {
		if (quantity.isUnlimitedOrUncomptable()) {
			return new StringBuilder(stockElement);
		} else {
			return buildIngredientName(elements);
		}
	}

	private StringBuilder buildIngredientName(String[] elements) {
		StringBuilder ingredientName = new StringBuilder();
		for (int i = 1; i < elements.length; i++) {
			ingredientName.append(elements[i]);
			if (i != elements.length - 1) {
				ingredientName.append(SPACE);
			}
		}
		return ingredientName;
	}

}
